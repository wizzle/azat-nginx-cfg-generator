#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
from dotenv import load_dotenv
from jinja2 import Template

basedir = os.path.abspath(os.path.dirname(__file__))
load_dotenv(os.path.join(basedir, '.env'))

template_file = os.environ.get('TEMPLATE_FILE_PATH') or None
domains_file = os.environ.get('DOMAINS_FILE_PATH') or None
destination_folder = os.environ.get('DESTINATION_FOLDER') or None


def _error(message):
    sys.exit(message)


def main():
    if template_file is None:
        _error('You should specify the TEMPLATE_FILE_PATH variable in the .env. Exiting.')

    if domains_file is None:
        _error('You should specify the DOMAINS_FILE_PATH variable in the .env. Exiting.')

    if destination_folder is None:
        _error('You should specify the DESTINATION_FOLDER variable in the .env. Exiting.')

    if os.access(destination_folder, os.W_OK | os.X_OK) is not True:
        _error('Insufficient privileges detected.\n'
               'Or destination folder doesn\'t exist.\n'
               'Please try again, this time using \'sudo\'. Exiting.')

    with open(os.path.join(basedir, template_file)) as file_:
        template = Template(file_.read())

    with open(os.path.join(basedir, domains_file)) as f:
        domains = f.readlines()

    for domain in domains:
        file_path = os.path.join(destination_folder, domain)
        dest_file = open(file_path, 'w+')
        nginx_config = template.render(domain=domain)
        dest_file.write(nginx_config)
        dest_file.close()


if __name__ == "__main__":
    main()
